'use strict';

angular.module('rbacAdminApp')
	.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/main');

		$stateProvider
			.state('main', {
				url: '/main',
				controller: 'MainCtrl',
				templateUrl: 'views/main.html'
			})
			.state('login', {
				url: '/login',
				controller: 'LoginCtrl',
				templateUrl: 'views/user/login.html'
			})
			.state('logout', {
				url: '/logout',
				controller: 'LogoutController',
			})
			.state('about', {
				url: '/about',
				templateUrl: 'views/about.html'
			});
	});