'use strict';

/**
 * @ngdoc overview
 * @name rbacAdminApp
 * @description
 * # rbacAdminApp
 *
 * Main module of the application.
 */
var App = angular
  .module('rbacAdminApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ]);

  // .config(function ($routeProvider) {
  //   $routeProvider
  //     .when('/', {
  //       templateUrl: 'views/main.html',
  //       controller: 'MainCtrl'
  //     })
  //     .when('/about', {
  //       templateUrl: 'views/about.html',
  //       controller: 'AboutCtrl'
  //     })
  //     .otherwise({
  //       redirectTo: '/'
  //     });
  // });

App.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/main');

    $stateProvider
      .state('main', {
        url: '/main',
        controller: 'MainCtrl',
        templateUrl: 'views/main.html'
      })
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'views/user/login.html'
      })
      .state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl',
      })
      .state('about', {
        url: '/about',
        controller: 'AboutCtrl',
        templateUrl: 'views/about.html'
      })
      .state('user', {
        url: '/user',
        controller: 'UserCtrl',
        templateUrl: 'views/user/list.html',
        data: {
          authenticate: true
        }
      })
       .state('resource', {
        url: '/resource',
        controller: 'ResourceCtrl',
        templateUrl: 'views/resource/index.html',
        data: {
          authenticate: true
        }
      })
      .state('resourceDetail', {
        url: '/resource_detail/:resourceId',
        controller: 'ResourceDetailCtrl',
        templateUrl: 'views/resource/detail.html',
        data: {
          authenticate: true
        }
      });
  });

App.constant('appConfiguration', {
    xAuthTokenHeaderName: 'x-auth-token'
  });

App.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  });

// App.config(['$routeProvider', '$locationProvider', function($routeProvider) {
//         $routeProvider.when('/login', {
//             templateUrl: 'views/user/login.html',
//             controller: 'UserCtrl'
//         });

//         $routeProvider.when('/main', {
//             templateUrl: 'views/main.html',
//             controller: 'MainCtrl'
//         });

//         $routeProvider.otherwise({redirectTo: '/login'});

//     }]);

App.config(function ($httpProvider) {
    /* Intercept http errors */
    var interceptor = function ($rootScope, $q, $location) {

      function success(response) {
        return response;
      }

      function error(response) {

        var status = response.status;
        var config = response.config;
        var method = config.method;
        var url = config.url;

        if (status === 401) {
          $location.path('/login');
        } else {
          $rootScope.error = method + ' on ' + url + ' failed with status ' + status;
        }

        return $q.reject(response);
      }

      return function (promise) {
          return promise.then(success, error);
      };
    };
    $httpProvider.responseInterceptors.push(interceptor);
  });

App.run(function($rootScope, $http, $state, $cookieStore, appConfiguration, $log) {
    /* Reset error when a new view is loaded */
    $rootScope.$on('$viewContentLoaded', function() {
      delete $rootScope.error;
    });
    $rootScope.$on('$stateChangeStart', function(event, toState) {
      if (toState.data !== undefined && toState.data.authenticate !== undefined) {
        $log.info('Need to authenticate', toState);
        $log.info('User authenticated?', $rootScope.isAuthenticated());
        //appConfiguration.authenticationEnabled && 
        if (toState.data.authenticate && !$rootScope.isAuthenticated()){
          // User is not authenticated
          $log.info('Setting desired state to ' + toState.name);
          $rootScope.desiredToState = toState.name;
          $state.transitionTo('login');
          event.preventDefault();
        }
      }
    });

  $rootScope.isAuthenticated = function() {

    if ($rootScope.user === undefined) {
      return false;
    }
    if ($rootScope.user.isAuthenticated === undefined) {
      return false;
    }
    if (!$rootScope.user.isAuthenticated) {
      return false;
    }
    return true;
  };
  $rootScope.hasRole = function(role) {

    if (!$rootScope.isAuthenticated()) {
      return false;
    }
    if ($rootScope.user.roles[role] === undefined) {
      return false;
    }
    return $rootScope.user.roles[role];
  };

//Login validation. 
//!!!!!! ATTENTION !!!!!!!
//If session id is expired, $rootScope.SessionID should be set to null or empty string!!!
// App.run(function($rootScope, $location) {

//     // register listener to watch route changes
//     $rootScope.$on("$routeChangeStart", function(event, next, current) {

//         console.log("Routechanged sessionId="+$rootScope.SessionId);

//         if ($rootScope.SessionId == '' || $rootScope.SessionId == null) {

//             // no logged user, we should be going to #login
//             if (next.templateUrl == "views/user/login.html") {
//                 // already going to #login, no redirect needed
//             } else {
//                 // not going to #login, we should redirect now
//                 $location.path("/login");
//             }
//         }
//     });
});
