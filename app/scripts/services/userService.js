'use strict';

angular.module('rbacAdminApp')
	.factory('UserService', function($resource, $http){
		return {
	  
  	  	productResource: $resource('rbac/product/:id'),
          get: function(param, callback){
            return this.productResource.get(param,callback);
          },
          query: function(){
            return this.productResource.query();
          },
          save: function(data){
            if(data.id != null){
              return $http.put('rbac/product/'+data.id, data);
            } else {
              return $http.post('rbac/product', data);
            }
          },
          remove: function(data){
            if(data.id != null){
              return $http.delete('rbac/product/'+data.id);
            }
          },

          getCategoryResource: $resource('rbac/category/:id'),
          getCategory: function(){
             return this.getCategoryResource.query();
          }

	    };
});