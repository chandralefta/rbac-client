'use strict';

angular.module('rbacAdminApp')
	.factory('ResourceService', function($resource, $http){
		return {
	      rbacResource: $resource('rbac/resource/:id'),
          get: function(param, callback){
            return this.rbacResource.get(param,callback);
          },
          query: function(){
            return this.rbacResource.query();
          },
          save: function(data){
            if(data.id != null){
                return $http.put('rbac/resource/'+data.id, data);
            } else {
                return $http.post('rbac/resource', data);
            }
          },
          remove: function(data){
            if(data.id != null){
              return $http.delete('rbac/resource/'+data.id);
            }
          },

          addUser: function(data){
          	console.log(data);
            return $http.put('rbac/resource/'+data.id, data);
          },

          getUserResource: $resource('rbac/user/:id'),
	      getUser: function(){
	      	 return this.getUserResource.query();
	      }

	    };
});