'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
