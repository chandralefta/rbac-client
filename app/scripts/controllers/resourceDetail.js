'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('ResourceDetailCtrl', function ($scope, $state, $stateParams, ResourceService) {

    $scope.user = ResourceService.getUser();

    var resourceId = $stateParams.resourceId;
   

    $scope.selectedResourceOld = ResourceService.get({id: resourceId}, function(result){
          $scope.original = angular.copy(result);
           
    }); 

    $scope.edit = function(x){
        $scope.selectedResource = ResourceService.get({id: x.id}, function(result){
              $scope.original = angular.copy(result);
        });
    };

    //console.log(  $scope.selectedResourceOld.$promise );

    // $scope.edit = function(x){
    //     $scope.selectedResource = ResourceService.get({id: x.id}, function(result){
    //           $scope.original = angular.copy(result);
    //     });
    // };

    $scope.save = function(){
        ResourceService.addUser($scope.selectedResource).success(function(){
            $scope.resource = ResourceService.query();
            $scope.create();
        });
    };

    $scope.create = function(){
        $scope.selectedResource.id = $scope.selectedResourceOld.id;
        $scope.selectedResource.users = null;
        $scope.original = null;
    };

    // $scope.delete = function(x){
    //     ResourceService.remove(x).success(function(){
    //         $scope.resource = ResourceService.query();
    //         $scope.create();
    //     });
    // };

    $scope.changedUser = function() {
        $scope.userNew = $scope.user;
    };


  });