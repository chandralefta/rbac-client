'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('LoginCtrl', function ($log, $scope, $rootScope, $state, $http, $cookieStore, $cookies, AuthService, appConfiguration) {
    
    $scope.user = {};
    $scope.user.username = '';
    $scope.user.password = '';

    $scope.loginUser = function(user) {

        AuthService.login(user).success(function(login) {
        console.log(login);                       
            $log.info('got user:', login);
            $rootScope.user = login;
            $cookieStore.put('isAuthenticated', true);
            $rootScope.user.isAuthenticated = true || $cookies.isAuthenticated;
            console.log(login.token);
            $http.defaults.headers.common[appConfiguration.xAuthTokenHeaderName] = login.token;
            //For a 'remember me' experience
            //$cookieStore.put('user', user);
            if ($rootScope.desiredToState !== undefined) {
                $state.go($rootScope.desiredToState);
                $rootScope.desiredToState = undefined;
            }
            else {
                $state.go('about');
            }

        }).error(function() {
            $scope.setError('Invalid user/password combination');
        });  
    };

    $scope.resetError = function() {
        $scope.error = false;
        $scope.errorMessage = '';
    };

    $scope.setError = function(message) {
        $scope.error = true;
        $scope.errorMessage = message;
        $rootScope.SessionId='';
    };

  })

.controller('LogoutCtrl', function ($log, $rootScope, $http, $cookieStore, appConfiguration, $state, AuthService) {
        $log.info('Logging out...');
        delete $rootScope.user;
        delete $http.defaults.headers.common[appConfiguration.xAuthTokenHeaderName];
        AuthService.logout();
        $cookieStore.remove('isAuthenticated');
        $state.go('main');
    });