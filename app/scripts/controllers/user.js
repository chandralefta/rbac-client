'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('UserCtrl', function ($scope, UserService) {
    
    $scope.products = UserService.query();
    $scope.OptCategory = UserService.getCategory();

    $scope.edit = function(x){
        $scope.selectedProduct = UserService.get({id: x.id}, function(result){
              $scope.original = angular.copy(result);
        });
    };

    $scope.save = function(){
        UserService.save($scope.selectedProduct).success(function(){
            $scope.products = UserService.query();
            $scope.create();
        });
    };

    $scope.create = function(){
        $scope.selectedProduct = null;
        $scope.original = null;
    };

    $scope.delete = function(x){
        UserService.remove(x).success(function(){
            $scope.products = UserService.query();
            $scope.create();
        });
    };

    $scope.changedCategory = function() {
        $scope.OptCategoryNew = $scope.OptCategory;
    };

  });
