'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('ResourceCtrl', function ($scope, ResourceService) {

   $scope.resource = ResourceService.query();

    $scope.edit = function(x){
        $scope.selectedResource = ResourceService.get({id: x.id}, function(result){
              $scope.original = angular.copy(result);
        });
    };

    $scope.save = function(){
        ResourceService.save($scope.selectedResource).success(function(){
            $scope.resource = ResourceService.query();
            $scope.create();
        });
    };

    $scope.create = function(){
        $scope.selectedResource = null;
        $scope.original = null;
    };

    $scope.delete = function(x){
        ResourceService.remove(x).success(function(){
            $scope.resource = ResourceService.query();
            $scope.create();
        });
    };

  });