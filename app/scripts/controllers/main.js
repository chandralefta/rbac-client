'use strict';

/**
 * @ngdoc function
 * @name rbacAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rbacAdminApp
 */
angular.module('rbacAdminApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
